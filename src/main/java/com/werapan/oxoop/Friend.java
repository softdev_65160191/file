/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.oxoop;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Chantima
 */
public class Friend implements Serializable{
    private int id;
    private String name;
    private String tel;
    private int age;
    private static int lastId=1;

    public Friend(String name, int age ,String tel) {
        this.id = lastId++;
        this.name = name;
        this.tel = tel;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Friend{" + "id=" + id + ", name=" + name + ", tel=" + tel + ", age=" + age + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws Exception {
        if(age<0) {
            throw new Exception();
        }
        this.age = age;
    }

    public static int getLastId() {
        return lastId;
    }

    public static void setLastId(int lastId) {
        Friend.lastId = lastId;
    }
        
    
    public static void main(String[] args){
        Friend f1 = new Friend("Pom",70,"0811111111");
        Friend f2 = new Friend("Pakat",70,"0811111112");
        System.out.println(f1.toString());
        System.out.print(f2);
        try {
            f1.setAge(-1);
        } catch (Exception ex) {
            System.out.println("Shit");
        }
       
    }
}
